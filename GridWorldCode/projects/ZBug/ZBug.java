import info.gridworld.actor.Bug;

public class ZBug extends Bug{
    private int steps;
    private int sideLength;
    private int x;
    public ZBug(int length)
    {
	turn();
	turn();
	x = 1;
        steps = 0;
        sideLength = length;
    }
    public void act()
    {
        if (x <= 3 && steps < sideLength){
	    if (canMove()){
		move();
		steps++;
	    }
	}
	else if (x == 1){
	    turn();
	    turn();
	    turn();
	    steps = 0;
	    x++;
	}
	else if (x ==2){
	    turn();
	    turn();
	    turn();
	    turn();
	    turn();
	    steps =0;
	    x++;
	}
    }
}
    
